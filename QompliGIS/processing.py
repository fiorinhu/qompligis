"""
QompliGIS provider
"""
import json
from pathlib import Path

import yaml
from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingMultiStepFeedback,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFile,
)

from QompliGIS.report import ReportFormat
from QompliGIS.utils import DoVerif, InputFormat, tr


class QompliGISProcessing(QgsProcessingAlgorithm):
    """Generate the report in the processing toolboox"""

    def initAlgorithm(self, config=None):
        """Initialize the algorithm.

        :param config:
        """
        self.addParameter(
            QgsProcessingParameterFile(
                "conf",
                tr("Configuration file"),
                behavior=QgsProcessingParameterFile.File,
                fileFilter="YAML (*.yaml)",
                defaultValue=None,
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "foldertocheck",
                tr("Data folder to check (for Shapefiles)"),
                behavior=QgsProcessingParameterFile.Folder,
                optional=True,
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "filetocheck",
                tr("Data file to check"),
                behavior=QgsProcessingParameterFile.File,
                fileFilter=";;".join([a.value for a in InputFormat]),
                optional=True,
            )
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                "report_format",
                tr("Report format"),
                options=["Markdown", "HTML", "JSON"],
                allowMultiple=False,
                defaultValue=[],
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                "report_path",
                tr("Report path"),
                behavior=QgsProcessingParameterFile.File,
                defaultValue=None,
            )
        )
        # add parameter "file" for the CSS path
        self.addParameter(
            QgsProcessingParameterFile(
                "css_path",
                tr("CSS path"),
                behavior=QgsProcessingParameterFile.File,
                defaultValue=None,
                fileFilter="CSS (*.css)",
                optional=True,
            )
        )

    def processAlgorithm(self, parameters, context, model_feedback):
        """Process the algorithm.

        :param parameters:
        :param context:
        :param model_feedback:
        """

        confpath = self.parameterAsFile(parameters, "conf", context)
        filepath = self.parameterAsFile(parameters, "filetocheck", context)
        folderpath = self.parameterAsFile(parameters, "foldertocheck", context)
        if not (filepath or folderpath):
            raise QgsProcessingException(
                tr(
                    "A data file or a data folder to check (for Shapefiles)"
                    " must be supplied"
                )
            )
        with Path(confpath).open() as conf:
            conf_dict = yaml.full_load(conf)
        self.worker = DoVerif(conf_dict, filepath or folderpath)
        self.worker.run()

        report_path = self.parameterAsFile(parameters, "report_path", context)
        # assign to a variable the parameter
        report_format = self.parameterAsEnum(parameters, "report_format", context)
        css_path = self.parameterAsFile(parameters, "css_path", context)
        with Path(report_path).open("w", encoding="utf-8") as fileout:
            if report_format == 0:
                fileout.write(self.worker.run_report.report(ReportFormat.MARKDOWN))
            elif report_format == 1:
                fileout.write(
                    self.worker.run_report.report(ReportFormat.HTML, css_path)
                )
            # add JSON output
            elif report_format == 2:
                json.dump(
                    self.worker.run_report.report(ReportFormat.JSON),
                    fileout,
                    ensure_ascii=False,
                )
        results = {"log": tr("Saved to ") + report_path}

        return results

    # pylint: disable=R0201
    def name(self):
        """Processing name."""
        return "check_compliance"

    # pylint: disable=R0201
    def displayName(self):
        """Processing displayed and translated name."""
        return tr("Check compliance")

    # pylint: disable=R0201
    def group(self):
        """Processing group."""
        return "QompliGIS"

    # pylint: disable=R0201
    def groupId(self):
        """Processing groupId."""
        return "QompliGIS"

    # pylint: disable=R0201
    def createInstance(self):
        """Create instance."""
        return QompliGISProcessing()
