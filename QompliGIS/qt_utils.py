"""
Some qt utils functions
"""

from typing import cast

from qgis.PyQt.QtCore import QCoreApplication, Qt


def boolToQtCheckState(constraint: bool) -> Qt.CheckState:
    """From a bool return Qt.CheckState.

    :param constraint:
    :type constraint: bool
    :rtype: Qt.CheckState
    """
    return Qt.Checked if constraint else Qt.Unchecked


def qtCheckStateToBool(check_state: Qt.CheckState) -> bool:
    """From a Qt.CheckState return a boolean.

    :param check_state:
    :type check_state: Qt.CheckState
    :rtype: bool
    """
    return cast(bool, check_state == Qt.Checked)


def tr(text: str) -> str:
    """Translate the text using QCoreApplication.translate method.

    :param text:
    :type text: str
    :rtype: str
    """
    return cast(str, QCoreApplication.translate("@default", text))
