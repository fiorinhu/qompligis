FORMS = ../conf_wid.ui

SOURCES = ../qompligis.py \
  ../gui_utils.py \
  ../qt_utils.py \
  ../utils.py \
  ../report.py \
  ../processing.py \
  ../provider.py

TRANSLATIONS = fr.ts
