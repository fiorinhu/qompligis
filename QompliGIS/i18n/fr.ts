<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../qompligis.py" line="58"/>
        <source>Create a template</source>
        <translation>Créer une configuration</translation>
    </message>
    <message>
        <location filename="../qompligis.py" line="64"/>
        <source>Edit a template</source>
        <translation>Éditer une configuration</translation>
    </message>
    <message>
        <location filename="../qompligis.py" line="70"/>
        <source>Compliance check</source>
        <translation>Vérification de conformité</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="556"/>
        <source>Choose a file</source>
        <translation>Choisir un fichier</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="559"/>
        <source>Choose a folder</source>
        <translation>Choisir un dossier</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="93"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="525"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="343"/>
        <source>Constraints configuration</source>
        <translation>Configuration des contraintes</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="464"/>
        <source>Completed</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="479"/>
        <source>Save configuration file</source>
        <translation>Enregistrer le fichier de configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="480"/>
        <source>Choose the configuration file location and name</source>
        <translation>Choisir l&apos;emplacement et le nom du fichier de configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="499"/>
        <source>The file extension must be .yaml</source>
        <translation>L&apos;extension doit être .yaml</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="518"/>
        <source>Load the configuration file</source>
        <translation>Charger le fichier de configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="537"/>
        <source>Bad configuration file!</source>
        <translation>Fichier de configuration invalide !</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="542"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="492"/>
        <source>The path {ref_filepath} referenced in the config file does not exist!</source>
        <translation type="obsolete">Le chemin {ref_filepath} référencé dans le fichier de configuration n&apos;existe pas !</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../gui_utils.py" line="594"/>
=======
        <location filename="../report.py" line="181"/>
>>>>>>> html
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="613"/>
        <source>Save report</source>
        <translation>Enregistrer le rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="614"/>
        <source>Choose the path and format of the report</source>
        <translation>Choisir l&apos;emplacement et le format du rapport</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../gui_utils.py" line="655"/>
=======
        <location filename="../gui_utils.py" line="647"/>
>>>>>>> html
        <source>Finished</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="636"/>
        <source>&lt;html&gt;The report has correctly been saved to &lt;a href=&quot;{self.field(&apos;report_path&apos;)}&quot;&gt;{self.field(&apos;report_path&apos;)}&lt;/a&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;Le rapport a été correcement sauvegardé ici : &lt;a href=&quot;{self.field(&apos;report_path&apos;)}&quot;&gt;{self.field(&apos;report_path&apos;)}&lt;/a&gt;&lt;/html&gt;</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../gui_utils.py" line="671"/>
=======
        <location filename="../gui_utils.py" line="664"/>
>>>>>>> html
        <source>Configuration creation</source>
        <translation>Création de la configuration</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="67"/>
        <source>Choose the dataset to use as a reference</source>
        <translation>Choisir les données à utiliser comme référence</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../gui_utils.py" line="688"/>
=======
        <location filename="../gui_utils.py" line="681"/>
>>>>>>> html
        <source>Configuration edition</source>
        <translation>Modification de la configuration</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../gui_utils.py" line="690"/>
=======
        <location filename="../gui_utils.py" line="683"/>
>>>>>>> html
        <source>Edit configuration</source>
        <translation>Modifier la configuration</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../processing.py" line="116"/>
=======
        <location filename="../processing.py" line="124"/>
>>>>>>> html
        <source>Check compliance</source>
        <translation>Vérifier la conformité</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="124"/>
        <source>Choose the dataset to check</source>
        <translation>Choisir les données à vérifier</translation>
    </message>
    <message>
        <location filename="../utils.py" line="585"/>
        <source>The path {0} does not exist!</source>
        <translation>Le chemin {0} n&apos;existe pas !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="628"/>
        <source>No layer found in reference file!</source>
        <translation>Pas de couche trouvée dans le fichier de référence !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="643"/>
        <source>Checking layer list...</source>
        <translation>Vérification de la liste de couches...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="646"/>
        <source>OK: Layers comply with the reference</source>
        <translation>OK : les couches sont conformes à la référence</translation>
    </message>
    <message>
        <location filename="../utils.py" line="653"/>
        <source>Layer &apos;{0}&apos; is missing in dataset</source>
        <translation>La couche &apos;{0}&apos; est absente des données d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="657"/>
        <source>Layer &apos;{0}&apos; found but not present in reference dataset</source>
        <translation>La couche &apos;{0}&apos; a été trouvée, mais est absente des données de référence</translation>
    </message>
    <message>
        <location filename="../utils.py" line="673"/>
        <source>Checking layer {0}</source>
        <translation>Vérification de la couche {0}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="677"/>
        <source>Checking CRS...</source>
        <translation>Vérification du SCR...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="910"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../utils.py" line="684"/>
        <source>CRS is {new_crs} but must be {ref_crs}</source>
        <translation>Le SRC est {new_crs} mais doit être {ref_crs}</translation>
    </message>
    <message>
        <location filename="../utils.py" line="691"/>
        <source>No CRS check asked</source>
        <translation>Pas de vérification du SCR demandée</translation>
    </message>
    <message>
        <location filename="../utils.py" line="697"/>
        <source>Checking fields...</source>
        <translation>Vérification des champs...</translation>
    </message>
    <message>
        <location filename="../utils.py" line="709"/>
        <source>Differences found on fields: see each field information.</source>
        <translation>Différences trouvées sur les champs : voir le détail de chaque champ</translation>
    </message>
    <message>
        <location filename="../utils.py" line="714"/>
        <source>Missing</source>
        <translation>Manquant</translation>
    </message>
    <message>
        <location filename="../utils.py" line="718"/>
        <source> - AND MANDATORY!</source>
        <translation> - ET OBLIGATOIRE !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="720"/>
        <source>Not in reference layer.</source>
        <translation>Absent de la couche de référence</translation>
    </message>
    <message>
        <location filename="../utils.py" line="724"/>
        <source>Fields are in a different order: found {0} but reference layer order is {1}.</source>
        <translation>Les champs sont dans un ordre différent : {0} trouvé mais l&apos;ordre de référence est {1}.</translation>
    </message>
    <message>
        <location filename="../utils.py" line="728"/>
        <source>Fields are all present and in the same order: see each field information.</source>
        <translation>Les champs sont tous présents et dans le même ordre : voir le détail de chaque champ.</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="737"/>
=======
        <location filename="../utils.py" line="723"/>
>>>>>>> html
        <source>Length differ: found {0} but reference is {1}.</source>
        <translation>La longueur diffère : trouvé {0} mais la référence est {1}.</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="742"/>
=======
        <location filename="../utils.py" line="728"/>
>>>>>>> html
        <source>Precision differ: found {0} but reference is {1}.</source>
        <translation>La précision diffère : trouvée {0} mais la référence est {1}.</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="747"/>
=======
        <location filename="../utils.py" line="733"/>
>>>>>>> html
        <source>Type differ: found (type: {0}, length: {1}, precision: {2}) but reference is (type: {3}, length: {4}, precision: {5}).</source>
        <translation>Le type diffère : trouvé (type: {0}, longueur: {1}, précision: {2}) mais la référence est (type: {3}, longueur: {4}, précision: {5}).</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="760"/>
=======
        <location filename="../utils.py" line="746"/>
>>>>>>> html
        <source>Other difference... found informations are type: {0}, length: {1}, precision: {2} and reference informations are type: {3}, length: {4}, precision: {5}.</source>
        <translation>Autre différence... informations trouvées : type: {0}, longueur: {1}, précision: {2} et les informations de référence sont : type: {3}, longueur: {4}, précision: {5}.</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="772"/>
=======
        <location filename="../utils.py" line="758"/>
>>>>>>> html
        <source>Fields are the same.</source>
        <translation>Les champs sont identiques.</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="777"/>
=======
        <location filename="../utils.py" line="763"/>
>>>>>>> html
        <source>No strict check on fields</source>
        <translation>Pas de vérification stricte sur les champs.</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="800"/>
=======
        <location filename="../utils.py" line="786"/>
>>>>>>> html
        <source>Checking minimum length...</source>
        <translation>Vérification de la longueur minimale...</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="803"/>
=======
        <location filename="../utils.py" line="789"/>
>>>>>>> html
        <source>No minimum length check asked</source>
        <translation>Pas de vérification de longueur minimale demandée.</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="809"/>
=======
        <location filename="../utils.py" line="795"/>
>>>>>>> html
        <source>Checking minimum area...</source>
        <translation>Vérification de l&apos;aire minimale...</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="812"/>
=======
        <location filename="../utils.py" line="798"/>
>>>>>>> html
        <source>No minimum area check asked</source>
        <translation>Pas de vérification d&apos;aire minimale demandée.</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="818"/>
=======
        <location filename="../utils.py" line="804"/>
>>>>>>> html
        <source>Checking curves...</source>
        <translation>Vérification des courbes...</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="820"/>
=======
        <location filename="../utils.py" line="806"/>
>>>>>>> html
        <source>No curves check asked</source>
        <translation>Pas de vérification de courbes demandée</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="826"/>
=======
        <location filename="../utils.py" line="812"/>
>>>>>>> html
        <source>Checking holes...</source>
        <translation>Vérification des trous...</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="828"/>
=======
        <location filename="../utils.py" line="814"/>
>>>>>>> html
        <source>No holes check asked</source>
        <translation>Pas de vérification de trous demandée</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="839"/>
=======
        <location filename="../utils.py" line="826"/>
>>>>>>> html
        <source>Feature {0} has a length {1} &lt; minimum length {2}</source>
        <translation>L&apos;entité {0} a une longueur de {1} &lt; longueur minimum {2}</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="849"/>
=======
        <location filename="../utils.py" line="836"/>
>>>>>>> html
        <source>Feature {0} has an area {1} &lt; minimum area {2}</source>
        <translation>L&apos;entité {0} a une aire de {1} &lt; aire minimum {2}</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="859"/>
=======
        <location filename="../utils.py" line="846"/>
>>>>>>> html
        <source>Feature {0} contains curve(s)</source>
        <translation>L&apos;entité {0} contient une (des) courbe(s)</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="867"/>
=======
        <location filename="../utils.py" line="854"/>
>>>>>>> html
        <source>Feature {0} contains hole(s)</source>
        <translation>L&apos;entité {0} contient un (des) trou(s)</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="898"/>
=======
        <location filename="../utils.py" line="882"/>
>>>>>>> html
        <source>OK: layer complies with minimum length</source>
        <translation>OK : la couche est conforme à la longueur minimum</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="903"/>
=======
        <location filename="../utils.py" line="887"/>
>>>>>>> html
        <source>OK: layer complies with minimum area</source>
        <translation>OK : la couche est conforme à l&apos;aire minimum</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="908"/>
=======
        <location filename="../utils.py" line="892"/>
>>>>>>> html
        <source>OK: layer does not have curve</source>
        <translation>OK : la couche n&apos;a pas de courbe</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="913"/>
=======
        <location filename="../utils.py" line="897"/>
>>>>>>> html
        <source>OK: layer does not have hole</source>
        <translation>OK : la couche n&apos;a pas de trou</translation>
    </message>
    <message>
        <location filename="../report.py" line="44"/>
        <source>Layer list</source>
        <translation>Liste de couches</translation>
    </message>
    <message>
        <location filename="../report.py" line="46"/>
        <source>CRS</source>
        <translation>SCR</translation>
    </message>
    <message>
        <location filename="../report.py" line="47"/>
        <source>Fields</source>
        <translation>Champs</translation>
    </message>
    <message>
        <location filename="../report.py" line="48"/>
        <source>Curves</source>
        <translation>Courbes</translation>
    </message>
    <message>
        <location filename="../report.py" line="49"/>
        <source>Holes</source>
        <translation>Trous</translation>
    </message>
    <message>
        <location filename="../report.py" line="50"/>
        <source>Minimum length</source>
        <translation>Longueur minimum</translation>
    </message>
    <message>
        <location filename="../report.py" line="51"/>
        <source>Minimum area</source>
        <translation>Aire minimum</translation>
    </message>
    <message>
        <location filename="../report.py" line="84"/>
        <source>For FIELDS section, use addFieldInfo() method</source>
        <translation>Pour la section FIELDS, utiliser la méthode addFieldInfo()</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="138"/>
        <source>Choose a folder with your shapefiles</source>
        <translation>Choisir un dossier avec vos shapefiles</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="141"/>
        <source>Choose a file - {0}</source>
        <translation>Choisir un fichier - {0}</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="987"/>
=======
        <location filename="../utils.py" line="951"/>
>>>>>>> html
        <source>Bad formed configuration file: missing section(s) {0}</source>
        <translation>Fichier de configuration malformé : section(s) manquante(s) {0}</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="464"/>
        <source>The configuration file has been saved to {0}</source>
        <translation>Le fichier de configuration a été enregistré vers {0}</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="525"/>
        <source>The configuration file {0} does not exist!</source>
        <translation>Le fichier de configuration {0} n&apos;existe pas !</translation>
    </message>
    <message>
        <location filename="../utils.py" line="606"/>
        <source>The data format to check ({0}) does not correspond with the reference data ({1})</source>
        <translation>Le format de donnée à vérifier ({0}) ne correspond pas au format des données de référence ({1})</translation>
    </message>
    <message>
        <location filename="../report.py" line="90"/>
        <source>The layer {0} is not present in the report</source>
        <translation>La couche {0} est absente dans le rapport</translation>
    </message>
    <message>
        <location filename="../processing.py" line="27"/>
        <source>Configuration file</source>
        <translation>Fichier de configuration</translation>
    </message>
    <message>
        <location filename="../processing.py" line="36"/>
        <source>Data folder to check (for Shapefiles)</source>
        <translation>Dossier de données à vérifier (pour Shapefiles)</translation>
    </message>
    <message>
        <location filename="../processing.py" line="44"/>
        <source>Data file to check</source>
        <translation>Fichier de données à vérifier</translation>
    </message>
    <message>
        <location filename="../processing.py" line="53"/>
        <source>Report format</source>
        <translation>Format du rapport</translation>
    </message>
    <message>
        <location filename="../processing.py" line="62"/>
        <source>Report path</source>
        <translation>Chemin du rapport</translation>
    </message>
    <message>
        <location filename="../processing.py" line="93"/>
        <source>A data file or a data folder to check (for Shapefiles) must be supplied</source>
        <translation>Un fichier de données ou un dossier de données à vérifier (pour Shapefiles) doit être renseigné</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../processing.py" line="102"/>
=======
        <location filename="../processing.py" line="112"/>
>>>>>>> html
        <source>Saved to </source>
        <translation>Sauvegardé vers </translation>
    </message>
    <message>
        <location filename="../utils.py" line="722"/>
        <source>Field {0}: {1}</source>
        <translation>Champ {0} : {1}</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="834"/>
=======
        <location filename="../utils.py" line="820"/>
>>>>>>> html
        <source>Checking NULL fields...</source>
        <translation>Vérification des champs NULL...</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="887"/>
=======
        <location filename="../utils.py" line="871"/>
>>>>>>> html
        <source>Mandatory field is NULL in feature {0}</source>
        <translation>Champ obligatoire à NULL dans l&apos;entité {0}</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="542"/>
        <source>The path {ref_filepath} referenced in the config file does not exist!
Do you want to edit the path of the file/folder?</source>
        <translation type="obsolete">Le chemin {ref_filepath} referencé dans le fichier de configuration n&apos;existe pas !!
Souhaitez-vous éditer le chemin du fichier/dossier ?</translation>
    </message>
    <message>
<<<<<<< HEAD
        <location filename="../utils.py" line="926"/>
        <source>The dataset does not comply with the constraints (see details in report)</source>
        <translation>Les données ne sont pas conformes aux contraintes (voir détails dans le rapport)</translation>
    </message>
    <message>
        <location filename="../utils.py" line="930"/>
        <source>The dataset comply with the constraints</source>
        <translation>Les données sont conformes aux contraintes</translation>
    </message>
    <message>
        <location filename="../report.py" line="44"/>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="158"/>
        <source>The path {0} must be a folder!</source>
        <translation>Le chemin {0} doit être un dossier !</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="166"/>
        <source>The path {0} must be a file!</source>
        <translation>Le chemin {0} doit être un fichier !</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="655"/>
        <source>&lt;html&gt;The report has correctly been saved to &lt;a href=&quot;</source>
        <translation>&lt;html&gt;Le rapport a été correcement sauvegardé ici : &lt;a href=&quot;</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="711"/>
        <source>Save the report</source>
        <translation>Enregistrer le rapport</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="712"/>
        <source>Done</source>
        <translation>Terminé</translation>
=======
        <location filename="../gui_utils.py" line="602"/>
        <source>Report format :</source>
        <translation>Format du rapport :</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="605"/>
        <source>Report path :</source>
        <translation>Chemin du rapport :</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="622"/>
        <source>CSS path (optional):</source>
        <translation>Chemin du CSS (optionel) :</translation>
    </message>
    <message>
        <location filename="../processing.py" line="70"/>
        <source>CSS path</source>
        <translation>Chemin du CSS</translation>
    </message>
    <message>
        <location filename="../report.py" line="184"/>
        <source>Compliance Report</source>
        <translation>Rapport de vérification</translation>
    </message>
    <message>
        <location filename="../gui_utils.py" line="647"/>
        <source>&lt;html&gt;The report has correctly been saved to &lt;a href=&quot;{0}&quot;&gt;{0}&lt;/a&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;Le rapport a été correcement sauvegardé ici : &lt;a href=&quot;{0}&quot;&gt;{0}&lt;/a&gt;&lt;/html&gt;</translation>
>>>>>>> html
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../conf_wid.ui" line="20"/>
        <source>Which types of compliances do you want to configure ?</source>
        <translation>Quelles types de conformités voulez vous configurer ?</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="27"/>
        <source>Strict geometry type (Multi vs single, Z, M, etc)</source>
        <translation>Type de géométrie strict (Multi vs single, Z, M, etc)</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="37"/>
        <source>Don&apos;t allow a different coordinate reference system</source>
        <translation>Ne pas autoriser un système de coordonnées différent</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="44"/>
        <source>Don&apos;t allow new attributes</source>
        <translation>Ne pas autoriser de nouveaux attributs</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="52"/>
        <source>Attributes</source>
        <translation>Attributs</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="57"/>
        <source>Mandatory</source>
        <translation>Obligatoire</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="65"/>
        <source>Point layer options</source>
        <translation>Options de couche type Point</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="71"/>
        <source>No option yet</source>
        <translation>Pas d&apos;option pour le moment</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="81"/>
        <source>Line layer options</source>
        <translation>Options de couche type Ligne</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="131"/>
        <source>Can contain curves</source>
        <translation>Peut contenir des courbes</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="94"/>
        <source>Minimum length</source>
        <translation>Longueur minimale</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="111"/>
        <source>Polygon layer options</source>
        <translation>Option de couche type Polygone</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="124"/>
        <source>Minimum area</source>
        <translation>Aire minimum</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="138"/>
        <source>Can contain holes</source>
        <translation>Peut contenir des trous</translation>
    </message>
    <message>
        <location filename="../conf_wid.ui" line="14"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
</context>
</TS>
