import platform
from pathlib import Path

import pytest

from QompliGIS.report import LayerSection, MainSection, QompliGISReport, ReportFormat


def test_QompliGISReport():
    """
    Comparison of generated report with a reference
    """

    if platform.system() == "Linux":
        link = "/.local/share/QGIS/QGIS3/profiles/default/python/plugins/QompliGIS/resources/"
    elif platform.system() == "Windows":
        link = "/AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins/QompliGIS/resources/"
    # operating system MacOS is "Darwin"
    elif platform.system() == "Darwin":
        link = "/Library/Application Support/QGIS/QGIS3/profiles/default/python/plugins/QompliGIS/resources/"

    with Path(Path(__file__).parent / "testdata" / "test_report.html.ref").open(
        "w", encoding="utf-8"
    ) as fileout:
        fileout.write(
            '<head><link rel="stylesheet" href="'
            + str(Path.home())
            + link
            + 'css/styles.css"><link rel="icon" type="image/png" href="'
            + str(Path.home())
            + link
            + 'images/qompligis_logo.png"><title>Report</title><meta charset="utf-8"></head>\
        <div id="logo"><img id="logo-oslandia" src="'
            + str(Path.home())
            + link
            + 'images/logo_oslandia.png">\
        <img id="logo-qompligis" src="'
            + str(Path.home())
            + link
            + 'images/qompligis_logo.png"></div>\
        <h1 id="title">Compliance Report</h1><ul class="nav"><li><a href="#layer 1">layer 1</a></li></ul><h1 id="Layer list">Layer list</h1><p>ok ok</p><h1 id="layer 1">layer 1</h1><h2>Fields</h2><h3>super champ</h3><p>plutot correct</p><h2>Holes</h2><p>y\'a des trous</p>'
        )

    report = QompliGISReport()
    report.addLayer("layer 1")
    report.addFieldInfo("layer 1", "great field", "roughly correct")
    report.addLayerSection("layer 1", LayerSection.HOLES, "there are holes")
    report.addMainSection(MainSection.LAYER_LIST, "ok ok")
    report.addMainSection(MainSection.RESULT, "all is fine")
    with pytest.raises(ValueError) as e:
        report.addLayerSection("layer 1", LayerSection.FIELDS, "trial")
    assert str(e.value) == "For FIELDS section, use addFieldInfo() method"
    with pytest.raises(KeyError) as e:
        report.addLayerSection("layer 2", LayerSection.CRS, "trial")
    assert str(e.value) == "'The layer layer 2 is not present in the report'"
    assert (
        Path(__file__).parent / "testdata" / "test_report.html.ref"
    ).read_text() == report.report(
        ReportFormat.HTML
    ), "HTML report don't match with reference"
    assert (
        Path(__file__).parent / "testdata" / "test_report.md.ref"
    ).read_text() == report.report(
        ReportFormat.MARKDOWN
    ), "MARKDOWN report don't match with reference"
