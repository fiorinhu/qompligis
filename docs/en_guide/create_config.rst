Create compliance configuration file
====================================

Use the |create_config_logo| icon to open the configuration wizard:

.. image:: ../_static/images/en/create_1.png
   :width: 50%

#. Choose the dataset format among the supported types
#. Select the dataset file (or folder in the case of shapefile(s))
#. :guilabel:`Next`

.. image:: ../_static/images/en/create_2.png
   :width: 60%

#. Layers found in the reference dataset
#. General checks for the selected layer
#. Fields of the selected layer with checks that can be enabled for compliance
#. Geometry specific checks for the selected layer

.. image:: ../_static/images/en/create_3.png
   :width: 45%

#. Choose a path to save the configuration file
#. :guilabel:`Finish`

It is possible to edit an already existing configuration file with the icon |edit_config_logo|
and loading the file to edit.

.. |create_config_logo| image:: ../../QompliGIS/resources/images/newConfig.svg
   :width: 30 px

.. |edit_config_logo| image:: ../../QompliGIS/resources/images/editConfig.svg
   :width: 30 px
