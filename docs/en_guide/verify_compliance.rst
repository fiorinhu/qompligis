Verification of compliance
==========================

Use the |check_compliance_logo| icon to open the check compliance wizard.

#. Choose the configuration compliance file
#. Select the dataset file (or folder in the case of shapefiles)
#. The report page shows a live log of the verification

.. image:: ../_static/images/en/check_1.png
   :width: 50%

The full report is saved by selecting a format and a location on the last wizard page.

Supported formats are:

- HTML
- Markdown
- JSON

Example of a full report in Markdown:

.. image:: ../_static/images/en/check_2.png
   :width: 50%

.. figure:: ../_static/images/en/check_3.png
   :width: 100%

Example of a full report in HTML:

If you choose the HTML format, it is possible to load a CSS file in order to modify the report layout and design.

.. image:: ../_static/images/en/format_html_parametre.png
   :width: 50%

.. figure:: ../_static/images/en/html_report_preview.png
   :width: 100%

.. |check_compliance_logo| image:: ../../QompliGIS/resources/images/checkCompliance.svg
   :width: 30 px
