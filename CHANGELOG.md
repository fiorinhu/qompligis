# Changelog

## [1.1.0]

- Bug fix
- Processing now outputs a boolean for compliance check

## [1.0.3] - 2022/01/17

- Add a window to select a file/folder if the path is not correct.
- Minor code changes
